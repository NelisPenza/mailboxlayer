import supertest from 'supertest';
import { urls } from '../config';

const MailBoxLayer = function MailBoxLayer() {
    this.check = async function (access_key, email_address) {
        const response = supertest(urls.mailboxlayer)
            .post(`/check?access_key=${access_key}&email=${email_address}`);
        return response;
    };
};

export { MailBoxLayer };