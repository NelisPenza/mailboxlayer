import { MailBoxLayer } from '../framework/services';
import { expect, test } from '@jest/globals';

const access_key = 'a764a1517594963bf8981e61f4a4835f';

test('Положительный тест mailboxlayer', async () => {
    const email = 'nelis.penza@gmail.com';
    const response = await new MailBoxLayer().check(access_key, email);
    expect(response.status).toBe(200);
});

test.each`
    email                    | expected
    ${'cog.moscu@maec.es'}   | ${true}
    ${'cog.moscu@maec.e'}    | ${false}     
    `('Параметризованный тест mailboxlayer', async ({email, expected}) => {
    const response = await new MailBoxLayer().check(access_key, email);
    expect(response.status).toBe(200);
    expect(response.body.smtp_check).toEqual(expected);
});

test('Проверка права доступа к mailboxlayer без api_key', async () => {
    const access_key = '';
    const email = 'nelis.penza@gmail.com';
    const response = await new MailBoxLayer().check(access_key, email);
    expect(response.body.error.code).toBe(101);
});