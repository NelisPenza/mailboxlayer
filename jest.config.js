module.exports = {
  testEnvironment: 'node',
  reporters: [
    'default',  [
      'jest-html-reporters',
      {
        filename: 'index.html',
        expand: true,
        pageTitle: 'Сюда передаются результаты тестов'
      }
    ]

  ],
  moduleFileExtensions: ['js', 'json'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  testMatch: ['**/specs/*.spec.*'],
  globals: {
    testTimeout: 50000,
  },
  verbose: true,
  setupFilesAfterEnv: ['jest-allure/dist/setup']
};
